package net.intbird.soft.spring.mysql

import org.springframework.stereotype.Service

/**
 * created by intbird
 * on 2020/09/30
 */
@Service
class BookServiceImpl(val bookDao: IBookDao) : IBookService {
    override fun getAllBook(): List<Book>? {
        return bookDao.getAllBook()
    }

    override fun searchBook(id: Long): Book? {
        return bookDao.searchBook(id)
    }

    override fun insertBook(book: Book): Int? {
        return bookDao.insertBook(book)
    }

    override fun updateBook(id: Long,book: Book): Int? {
        return bookDao.updateBook(id,book)
    }

    override fun deleteBook(id: Long): Int? {
        return bookDao.deleteBook(id)
    }
}
