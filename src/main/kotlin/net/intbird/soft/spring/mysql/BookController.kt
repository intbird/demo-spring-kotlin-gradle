package net.intbird.soft.spring.mysql;

import org.springframework.web.bind.annotation.*

/**
 * https://docs.spring.io/spring-framework/docs/4.3.13.RELEASE/spring-framework-reference/html/jdbc.html
 */
@RestController
@RequestMapping("/book")
class BookController(var bookService: IBookService) {

    // http://127.0.0.1:8080/book/books
    @GetMapping(value = ["/books"])
    fun getAllBook(): List<Book>? {
        return bookService.getAllBook()
    }

    // http://127.0.0.1:8080/book/search?id=2
    @GetMapping(value = ["/search"])
    fun searchBook(id: Long): Book? {
        return bookService.searchBook(id)
    }

    // POST
    // http://127.0.0.1:8080/book/insert?id=3&name=intbird3&writer=intbird3
    @PostMapping(value = ["/insert"])
    fun insertBook(@ModelAttribute book: Book): Int? {
        return bookService.insertBook(book)
    }

    // PATCH
    // http://127.0.0.1:8080/book/update/1?name=intbird123
    @PatchMapping(value = ["/update/{id}"])
    fun updateBook(@PathVariable id: Long, @ModelAttribute book: Book): Int? {
        return bookService.updateBook(id,book)
    }

    // DELETE
    // http://127.0.0.1:8080/book/delete/3/
    @DeleteMapping(value = ["/delete/{id}"])
    fun deleteBook(@PathVariable id: Long): Int? {
        return bookService.deleteBook(id)
    }
}
