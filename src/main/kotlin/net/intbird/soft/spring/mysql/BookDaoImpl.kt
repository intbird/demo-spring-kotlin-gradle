package net.intbird.soft.spring.mysql


import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

/**
 * created by intbird
 * on 2020/09/30
 */
@Repository
class BookDaoImpl(val jdbcTemplate: JdbcTemplate) : IBookDao {

    override fun getAllBook(): List<Book>? {
        val sql = "SELECT id, name, writer FROM t_book"
        return jdbcTemplate.query(sql, BeanPropertyRowMapper(Book::class.java))
    }

    override fun searchBook(id: Long): Book? {
        val sql = "SELECT id, name, writer FROM t_book WHERE id=?"
        return jdbcTemplate?.queryForObject(sql, arrayOf(id), BeanPropertyRowMapper(Book::class.java, true))
    }

    override fun insertBook(book: Book): Int? {
        val sql = "INSERT INTO t_book(id,name,writer) VALUES(?,?,?)"
        return jdbcTemplate?.update(sql, book.id, book.name, book.writer)
    }

    override fun updateBook(id: Long, book: Book): Int? {
        val sql = "UPDATE t_book SET id=?, name=?, writer=? WHERE id=?"
        return jdbcTemplate?.update(sql, book.id, book.name, book.writer, id)
    }

    override fun deleteBook(id: Long): Int? {
        val sql = "DELETE FROM t_book WHERE id=?"
        return jdbcTemplate?.update(sql, id)
    }
}
