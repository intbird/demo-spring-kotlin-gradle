package net.intbird.soft.spring.mysql;

import java.io.Serializable

/**
 * created by intbird
 * on 2020/09/30
 */
data class Book(var id: Long = 0, var name: String = "", var writer: String = "") : Serializable