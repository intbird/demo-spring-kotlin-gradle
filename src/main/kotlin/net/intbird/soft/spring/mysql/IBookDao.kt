package net.intbird.soft.spring.mysql;

import net.intbird.soft.spring.mysql.Book

/**
 * created by intbird
 * on 2020/09/30
 */
interface IBookDao {

    fun getAllBook(): List<Book>?

    fun searchBook(id: Long): Book?

    fun insertBook(book: Book): Int?

    fun updateBook(id: Long, book: Book): Int?

    fun deleteBook(id: Long): Int?

}
